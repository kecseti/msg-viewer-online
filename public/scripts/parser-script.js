function formatEmail(data) {
    return data.name ? data.name + " [" + data.email + "]" : data.email;
}

function show(classOfElement) {
    Array.from(document.getElementsByClassName(classOfElement)).forEach((element) => element.style.display = 'block');
}

function hide(classOfElement) {
    Array.from(document.getElementsByClassName(classOfElement)).forEach((element) => element.style.display = 'none');
}

function addInnerHTML(classOfElement, htmlToInsert) {
    Array.from(document.getElementsByClassName(classOfElement)).forEach((element) => element.innerHTML = htmlToInsert);
}

function safeTags(str) {
    return str.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;') ;
}

function keepFormatting(str) {
    return str.replace(/\r\n/g,'</br>');
}

document.getElementById('file').addEventListener('change', event => {
    const selectedFile = event.target.files[0];
    if (!selectedFile) {
        hide('msg-info');
        return;
    }
    if (selectedFile.name.indexOf('.msg') == -1) {
        hide('msg-info');
        show('warning');
        return;
    }

    show('msg-info');
    hide('warning');

    const fileReader = new FileReader();
    fileReader.onload = (data) => {

        const buffer = data.target.result;
        const msgReader = new MSGReader(buffer);
        const fileData = msgReader.getFileData();
        if (!fileData.error) {
            addInnerHTML('sender-and-subject', `${formatEmail({name: fileData.senderName, email: fileData.senderEmail})}: ${fileData.subject}`);
            addInnerHTML('recipients', fileData.recipients.map((recipient) => formatEmail(recipient) + ' '));
            addInnerHTML('body', fileData.body ? keepFormatting(safeTags(fileData.body)) : '');
            
            if (fileData.attachments.length > 0) {
                const attachments = fileData.attachments.map((attachment, i) => {
                    const file = msgReader.getAttachment(i);
                    const fileUrl = URL.createObjectURL(new File([file.content], attachment.fileName, { type: attachment.mimeType ? attachment.mimeType : "application/octet-stream" }));
                    return `
                        <article>
                            <header>${attachment.fileName}</header>
                            <a href="${fileUrl}" download>Download</a>
                        </article>
                    `;
                });
                addInnerHTML('attachment-container', attachments);
            } else {
                hide('attachment-accordion');
            }
        } else {
            hide('msg-info');
            show('warning');
        }
    };

    fileReader.readAsArrayBuffer(selectedFile);
});